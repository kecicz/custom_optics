import numpy as np


def distances_from_similarities(similarity_matrix: np.ndarray) -> np.ndarray:
    similarity_matrix *= -1
    similarity_matrix += 1
    np.clip(similarity_matrix, 0, 2, out=similarity_matrix)
    np.fill_diagonal(similarity_matrix, 0)
    return similarity_matrix
