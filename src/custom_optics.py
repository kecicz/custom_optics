import numpy as np
import warnings
from sklearn.cluster._optics import cluster_optics_xi, cluster_optics_dbscan
from sklearn.neighbors import NearestNeighbors
from typing import Tuple, Optional


class CustomOptics:
    def __init__(self, cluster_method: str = 'xi', xi: float = 0.05, min_samples: int = 5):
        self.ordering_ = np.ndarray([])
        self.core_distances_ = np.ndarray([])
        self.reachability_ = np.ndarray([])
        self.predecessor_ = np.ndarray([])
        self.labels_ = np.ndarray([])
        self.min_samples = min_samples
        self.cluster_method = cluster_method
        self.xi = xi
        self.nbrs = NearestNeighbors(n_neighbors=self.min_samples, metric='precomputed')

    def radius_neighbors(self, distances: np.ndarray, radius: float) -> np.ndarray:
        indices = np.array([], dtype=np.int64)
        for idx, distance in enumerate(distances):
            if distance <= radius:
                indices = np.append(indices, int(idx))
        return indices

    def _slice_distances(self, X: np.ndarray, point_index: int) -> np.ndarray:
        similarity_slice = X[point_index, :].dot(X.T)
        similarity_slice *= -1
        similarity_slice += 1
        np.clip(similarity_slice, 0, 2, out=similarity_slice)
        similarity_slice[point_index] = 0
        return similarity_slice

    def n_neighbours_distance(self, dist: np.ndarray) -> float:
        neigh_ind = np.argpartition(dist, self.min_samples - 1, axis=0)
        neigh_ind = neigh_ind[:self.min_samples]
        # argpartition doesn't guarantee sorted order, so we sort again
        neigh_ind = neigh_ind[np.argsort(dist[neigh_ind])]
        return dist[neigh_ind][self.min_samples - 1]

    def _set_reach_dist(self, core_distances_: np.ndarray, reachability_: np.ndarray, predecessor_: np.ndarray,
                        point_index: int, processed: np.ndarray, P: np.ndarray) -> None:
        # max_eps is infinity - so we do not need to search for radius neighbors
        # indices = self.nbrs.radius_neighbors([P], radius=max_eps, return_distance=False)[0]
        indices = np.arange(P.shape[0])[:]
        # Getting indices of neighbors that have not been processed
        unproc = np.compress(~np.take(processed, indices), indices)
        # Neighbors of current point are already processed.
        if not unproc.size:
            return
        dists = P[unproc]
        # adjust distances, so all are greater than or equal to core_distance of the element
        rdists = np.maximum(dists, core_distances_[point_index])
        np.around(rdists, decimals=np.finfo(rdists.dtype).precision, out=rdists)
        # take all points, that have improved their reachability (distance to closest known neighbour)
        improved = np.where(rdists < np.take(reachability_, unproc))
        reachability_[unproc[improved]] = rdists[improved]
        predecessor_[unproc[improved]] = point_index

    def _compute_core_distances_(self, X: np.ndarray) -> np.ndarray:
        n_samples = X.shape[0]
        core_distances = np.empty(n_samples)
        core_distances.fill(np.nan)
        # assume samples always fit to memory
        # get distance to the 5th closest sample for each sample - could be done sequentially to reduce memory needs
        for idx in range(n_samples):
            distances = self._slice_distances(X, idx)
            core_distances[idx] = self.n_neighbours_distance(distances)
        return core_distances

    def compute_optics_graph(self, X: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
        n_samples = X.shape[0]
        # self.nbrs.fit(np.zeros((n_samples, n_samples)))
        reachability_ = np.empty(n_samples)
        reachability_.fill(np.inf)
        predecessor_ = np.empty(n_samples, dtype=int)
        predecessor_.fill(-1)

        core_distances_ = self._compute_core_distances_(X=X)
        np.around(
            core_distances_,
            decimals=np.finfo(core_distances_.dtype).precision,
            out=core_distances_,
        )
        processed = np.zeros(X.shape[0], dtype=bool)
        ordering = np.zeros(X.shape[0], dtype=int)
        for ordering_idx in range(n_samples):
            # Choose next based on smallest reachability distance
            # (And prefer smaller ids on ties, possibly np.inf!)
            index = np.where(processed == 0)[0]
            point = index[np.argmin(reachability_[index])]

            processed[point] = True
            ordering[ordering_idx] = point

            slice_distances = self._slice_distances(X, point)

            self._set_reach_dist(
                core_distances_=core_distances_,
                reachability_=reachability_,
                predecessor_=predecessor_,
                point_index=point,
                processed=processed,
                P=slice_distances
            )
        if np.all(np.isinf(reachability_)):
            warnings.warn(
                "All reachability values are inf. Set a larger"
                " max_eps or all data will be considered outliers.",
                UserWarning,
            )
        return ordering, core_distances_, reachability_, predecessor_

    def fit(self, X: np.ndarray):
        (
            self.ordering_,
            self.core_distances_,
            self.reachability_,
            self.predecessor_,
        ) = self.compute_optics_graph(X)

        # Extract clusters from the calculated orders and reachability
        if self.cluster_method == "xi":
            labels_, clusters_ = cluster_optics_xi(
                reachability=self.reachability_,
                predecessor=self.predecessor_,
                ordering=self.ordering_,
                min_samples=self.min_samples,
                min_cluster_size=None,
                xi=self.xi,
                predecessor_correction=True,
            )
            self.cluster_hierarchy_ = clusters_
        elif self.cluster_method == "dbscan":
            eps = np.inf

            labels_ = cluster_optics_dbscan(
                reachability=self.reachability_,
                core_distances=self.core_distances_,
                ordering=self.ordering_,
                eps=eps,
            )

        self.labels_ = labels_
        return self

    def fit_predict(self, X: np.ndarray, _y: Optional[np.ndarray] = None) -> np.ndarray:
        self.fit(X)
        return self.labels_
