import numpy as np

from timeit import default_timer as timer
from sklearn.preprocessing import normalize
from sklearn.cluster import OPTICS
from src.utils import distances_from_similarities
from src.custom_optics import CustomOptics

if __name__ == '__main__':
    feature_matrix: np.ndarray = np.random.rand(1_000, 3072)
    feature_matrix = normalize(feature_matrix, copy=False)

    start = timer()
    labels = CustomOptics(cluster_method='xi').fit_predict(feature_matrix)
    end = timer()
    elapsed_time = end - start

    feature_matrix_T = feature_matrix.T
    distance_matrix = distances_from_similarities(feature_matrix.dot(feature_matrix_T))

    # Original optics
    # labels = OPTICS(metric='precomputed', cluster_method='xi').fit_predict(distance_matrix)
    print("Optics took {} seconds".format(elapsed_time))

