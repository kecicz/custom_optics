from hypothesis import given, strategies as st
from hypothesis.extra.numpy import arrays
import numpy as np
from numpy.testing import assert_equal, assert_approx_equal
from sklearn.cluster import OPTICS
from sklearn.cluster._optics import compute_optics_graph
from sklearn.preprocessing import normalize
from src.utils import distances_from_similarities
from src.custom_optics import CustomOptics


def call_scikit(feature_matrix: np.ndarray) -> np.ndarray:
    feature_matrix_T = feature_matrix.T
    distance_matrix = distances_from_similarities(feature_matrix.dot(feature_matrix_T))
    return OPTICS(metric='precomputed').fit_predict(distance_matrix)


def call_custom(feature_matrix: np.ndarray) -> np.ndarray:
    return CustomOptics().fit_predict(feature_matrix)


@given(arrays(np.int, (100, 3072), elements=st.integers(-1023, 1023)))
def test_custom_predecessor_array_equals_scikit(feature_matrix: np.ndarray):
    feature_matrix = normalize(feature_matrix, copy=False)
    feature_matrix_T = feature_matrix.T
    distance_matrix = distances_from_similarities(feature_matrix.dot(feature_matrix_T))
    (_ordering, _core_distances, _reachability, sk_predecessor) = compute_optics_graph(
        X=distance_matrix,
        min_samples=5,
        algorithm='auto',
        leaf_size=30,
        metric='precomputed',
        metric_params=None,
        p=2,
        n_jobs=None,
        max_eps=np.inf,
    )
    custom_optics = CustomOptics()
    (
        _ordering,
        _core_distances,
        _reachability,
        custom_predecessor
    ) = custom_optics.compute_optics_graph(X=feature_matrix)
    assert_equal(custom_predecessor, sk_predecessor, verbose=True)


@given(arrays(np.int, (100, 3072), elements=st.integers(-1023, 1023)))
def test_custom_ordering_array_equals_scikit(feature_matrix: np.ndarray):
    feature_matrix = normalize(feature_matrix, copy=False)
    feature_matrix_T = feature_matrix.T
    distance_matrix = distances_from_similarities(feature_matrix.dot(feature_matrix_T))
    (sk_ordering, _core_distances, _reachability, _predecessor) = compute_optics_graph(
        X=distance_matrix,
        min_samples=5,
        algorithm='auto',
        leaf_size=30,
        metric='precomputed',
        metric_params=None,
        p=2,
        n_jobs=None,
        max_eps=np.inf,
    )
    custom_optics = CustomOptics()
    (
        custom_ordering,
        _core_distances,
        _reachability,
        _predecessor
    ) = custom_optics.compute_optics_graph(X=feature_matrix)
    assert_equal(custom_ordering, sk_ordering, verbose=True)


@given(arrays(np.float, 100, elements=st.floats(min_value=0, allow_nan=False, allow_infinity=False, exclude_min=True)))
def test_core_distance(dist: np.ndarray):
    custom_optics = CustomOptics()
    core_distance = custom_optics.n_neighbours_distance(dist)
    sorted_core_distance = np.sort(dist)[4]
    assert_approx_equal(core_distance, sorted_core_distance)
